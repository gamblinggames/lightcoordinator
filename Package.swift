// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LightCoordinator",
	platforms: [
		.iOS(.v13)
	],
    products: [
        .library(
            name: "LightCoordinator",
            targets: ["LightCoordinator"]),
    ],
    dependencies: [
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "LightCoordinator",
            dependencies: []),
    ]
)
