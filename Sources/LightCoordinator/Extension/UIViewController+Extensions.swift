//
//  UIViewController+Creation.swift
//  Worker Dashy
//
//  Created by Umbrella techon 16.08.17.
//  Copyright © 2017 Umbrella. All rights reserved.
//

import UIKit

public typealias StoryboardName = String

public extension UIViewController {
    static func controllerInStoryboard(_ storyboard: UIStoryboard, identifier: String) -> Self {
        return instantiateControllerInStoryboard(storyboard, identifier: identifier)
    }
    
    static func controllerInStoryboard(_ storyboard: UIStoryboard) -> Self {
        return controllerInStoryboard(storyboard, identifier: nameOfClass())
    }
    
    static func controllerFromStoryboard(_ storyboard: StoryboardName) -> Self {
        return controllerInStoryboard(UIStoryboard(name: storyboard, bundle: nil), identifier: nameOfClass())
    }
}

private extension UIViewController {
    static func instantiateControllerInStoryboard<T: UIViewController>(_ storyboard: UIStoryboard, identifier: String) -> T {
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }
}

extension NSObject {
	static func nameOfClass() -> String {
		return NSStringFromClass(self).components(separatedBy: ".").last!
	}
}
