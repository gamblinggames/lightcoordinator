//
//  Coordinatable.swift
//

import Foundation

public protocol Coordinatable: AnyObject {
	func start()
}
